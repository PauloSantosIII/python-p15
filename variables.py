import platform

build = [item.upper() for item in platform.python_build()]
compiler = platform.python_compiler()
processor = platform.processor()
system = platform.system()
machine = platform.machine()
release = platform.release()
version = platform.python_version()
uname = ' |*| '.join([item.lower() for item in platform.uname()])