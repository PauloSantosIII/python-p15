from flask import Flask
from environs import Env
from variables import *

env = Env()
env.read_env()

app = Flask(__name__)

@app.route('/')
def index():

    result = dict(
        build_do_python=build,
        compilador_do_python=compiler,
        processador=processor,
        release_do_sistema=release,
        versao_do_python=version,
        sistema=system,
        tipo_da_maquina=machine,
        uname=uname
    )

    return result
